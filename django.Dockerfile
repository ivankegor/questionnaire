FROM python:3.6
ENV wd /code
WORKDIR ${wd}
ADD requirements.txt ${wd}/requirements.txt
RUN pip install -r requirements.txt

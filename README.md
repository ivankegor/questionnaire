###Зависимости: 
postgres10, redis, python3.6

###Python зависимости: 
```bash
pip install -r requirements.txt
```

###Запуск django
Добавить в settings, local.py (пример local-tpl.py)
```bash
./manage.py migrate
./manage.py runserver
```
###Запуск celery
```bash
celery -A cindicator worker --loglevel INFO
```
###ИЛИ Docker
```bash
cd docker
docker-compose up
```
####Сделать миграции в docker:
```bash
docker exec -it docker_project_1 bash
./manage.py migrate
```
###Документация API
/docs
###Создание пользователя
POST /auth/registration/ 
```json
{
    "username": "admin",
    "password1": "123456",
    "password2": "123456",
    "is_admin": true
}
```
Ответ: 
```json
{"key": "<secret_key>"}
```
Добавить в хедер: Authorization: Token <secret_key>


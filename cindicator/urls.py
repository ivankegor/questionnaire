from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers

from cindicator.apps.questions.views import QuestionViewSet

schema_view = get_schema_view(
   openapi.Info(
      title="API",
      default_version='v1',
      description="API for service",
   ),
   public=True,
)

router = routers.SimpleRouter()
router.register('question', QuestionViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('rest_auth.urls')),
    path('auth/registration/', include('rest_auth.registration.urls')),
    path('docs/', schema_view.with_ui('redoc', cache_timeout=None), name='schema-redoc'),
    path('api/', include(router.urls))
]

from .base import *

if os.environ.get('DOCKER', False):
    from .docker import *

try:
    from .local import *
except ImportError:
    pass

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cindicator',
        'USER': 'cindicator',
        'PASSWORD': 'cindicator',
        'HOST': 'postgresql',  # Set to empty string for localhost.
        'PORT': '5432',  # Set to empty string for default.
        'CONN_MAX_AGE': 600,  # number of seconds database connections should persist for
    }
}


REDIS_HOST = 'redis'
REDIS_PORT = '6379'
BROKER_URL = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/0'
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600}
CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/0'

ALLOWED_HOSTS = ['*']
AUTH_PASSWORD_VALIDATORS = []

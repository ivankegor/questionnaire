from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from cindicator.apps.questions.filters import QuestionFilter
from cindicator.apps.questions.models import Question, Answer
from cindicator.apps.questions.permissions import AnswerCreatePermission
from cindicator.apps.questions.serializers import QuestionCreateSerializer, QuestionListSerializer, \
    AnswerCreateSerializer


class QuestionViewSet(mixins.CreateModelMixin,
                      mixins.ListModelMixin,
                      GenericViewSet):
    model = Question
    queryset = Question.objects.all()
    permission_classes = (IsAuthenticated,)
    filter_class = QuestionFilter

    def get_serializer_class(self):
        if self.action == 'create':
            return QuestionCreateSerializer
        elif self.action == 'list':
            return QuestionListSerializer
        elif self.action == 'answer':
            return AnswerCreateSerializer

    def get_permissions(self):
        permissions = super().get_permissions()
        if self.action == 'create':
            return permissions + [IsAdminUser()]
        return permissions

    @action(detail=True, methods=['post'], permission_classes=[AnswerCreatePermission])
    def answer(self, request, *args, **kwargs):
        question = self.get_object()
        answer = Answer(question=question, author=request.user)
        serializer = self.get_serializer(data=request.data, instance=answer)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)


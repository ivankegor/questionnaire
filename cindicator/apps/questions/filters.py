import django_filters
from django.utils import timezone
from django_filters.rest_framework import BooleanFilter

from cindicator.apps.questions.models import Question


class QuestionFilter(django_filters.FilterSet):
    is_actual = BooleanFilter(method='filter_is_actual')
    is_answered = BooleanFilter(method='filter_is_answered')
    title = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Question
        fields = ('title', 'end_time')

    def filter_is_actual(self, queryset, name, value):
        now = timezone.now()
        if value:
            return queryset.filter(end_time__gte=now)
        else:
            return queryset.filter(end_time__lt=now)

    def filter_is_answered(self, queryset, name, value):
        user = self.request.user
        if value:
            return queryset.filter(answers__author=user).distinct()
        else:
            return queryset.exclude(answers__author=user).distinct()

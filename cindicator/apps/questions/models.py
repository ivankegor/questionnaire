import datetime

from django.db import models
from django.utils import timezone
from rest_framework.compat import MaxValueValidator

from cindicator.apps.core.validators import NotEqualValidator


class Question(models.Model):
    title = models.TextField(verbose_name='Вопрос')
    end_time = models.DateTimeField(verbose_name='Время окончания')
    real_answer = models.PositiveSmallIntegerField(
        verbose_name='Реальный результат события',
        validators=[MaxValueValidator(100)],
        null=True
    )

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

    def can_create_answer(self, user):
        now = timezone.now()
        if self.end_time < now:
            return False, 'Время ответа на вопрос кончилось'

        answers = self.answers.filter(author=user).count()
        if answers >= Answer.MAX_ANSWERS:
            return False, f'Дано максимальное количество ответов ({answers})'

        if self.answers.filter(author=user, created_at__lt=now - datetime.timedelta(hours=1)).exists():
            return False, 'С последнего ответа прошло больше часа'

        return True, ''


class Answer(models.Model):
    MAX_ANSWERS = 2

    question = models.ForeignKey('questions.Question', on_delete=models.CASCADE, related_name='answers')
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    probability = models.PositiveSmallIntegerField(
        verbose_name='Вероятность',
        validators=[MaxValueValidator(100), NotEqualValidator(50)]
    )
    created_at = models.DateTimeField(verbose_name='Время ответа', auto_now=True)

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'

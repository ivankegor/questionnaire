# Generated by Django 2.1.1 on 2018-09-09 14:25

import cindicator.apps.core.validators
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import rest_framework.compat


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('probability', models.PositiveSmallIntegerField(validators=[rest_framework.compat.MaxValueValidator(100), cindicator.apps.core.validators.NotEqualValidator(50)], verbose_name='Вероятность')),
                ('created_at', models.DateTimeField(auto_now=True, verbose_name='Время ответа')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Ответ',
                'verbose_name_plural': 'Ответы',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.TextField(verbose_name='Вопрос')),
                ('end_time', models.DateTimeField(verbose_name='Время окончания')),
                ('real_answer', models.PositiveSmallIntegerField(null=True, validators=[rest_framework.compat.MaxValueValidator(100)], verbose_name='Реальный результат события')),
            ],
            options={
                'verbose_name': 'Вопрос',
                'verbose_name_plural': 'Вопросы',
            },
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='answers', to='questions.Question'),
        ),
    ]

from cindicator.apps.questions.tasks import update_user_answer_stat


def signal_update_user_answer_stat(instance, **kwargs):
    print('signal')
    update_user_answer_stat.delay(instance.author_id, instance.pk)

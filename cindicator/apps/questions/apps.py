from django.apps import AppConfig
from django.db.models.signals import post_save

from cindicator.apps.questions.signals import signal_update_user_answer_stat


class QuestionsConfig(AppConfig):
    name = 'cindicator.apps.questions'

    def ready(self):
        Answer = self.get_model('Answer')

        post_save.connect(signal_update_user_answer_stat, sender=Answer, weak=False)

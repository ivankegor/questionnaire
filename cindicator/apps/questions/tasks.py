import os

from django.conf import settings

from cindicator.celery import app


@app.task
def update_user_answer_stat(user_id, answer_id):
    with open(os.path.join(settings.PROJECT_DIR, 'celery-log.txt'), mode='a') as f:
        print(user_id, answer_id, file=f)

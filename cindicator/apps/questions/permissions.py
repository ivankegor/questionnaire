from rest_framework.permissions import BasePermission


class AnswerCreatePermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        perm, self.message = obj.can_create_answer(request.user)
        return perm


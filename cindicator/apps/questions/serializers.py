from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from cindicator.apps.questions.models import Question, Answer


class QuestionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('title', 'end_time')


class QuestionListSerializer(serializers.ModelSerializer):
    user_answer = serializers.SerializerMethodField()
    can_edit = serializers.SerializerMethodField()

    class Meta:
        model = Question
        fields = ('id', 'title', 'end_time', 'user_answer', 'real_answer', 'can_edit')

    def get_user_answer(self, question):
        user = self.context['request'].user
        try:
            return question.answers.filter(author=user).latest('created_at').probability
        except ObjectDoesNotExist:
            pass

    def get_can_edit(self, question):
        return question.can_create_answer(self.context['request'].user)[0]


class AnswerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('probability',)

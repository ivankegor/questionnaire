from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers


class CindiRegisterSerializer(RegisterSerializer):
    is_admin = serializers.BooleanField()

    def get_cleaned_data(self):
        cleaned_data = super().get_cleaned_data()
        cleaned_data['is_staff'] = self.validated_data.get('is_admin', False)
        return cleaned_data

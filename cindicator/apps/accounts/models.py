from django.db import models


class UserAnswerStat(models.Model):
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    common_question_count = models.IntegerField(
        verbose_name='Общее количество вопросов, на которые мог ответить пользователь',
        default=0
    )
    common_answer_count = models.IntegerField(verbose_name='Общее количество ответов', default=0)

    class Meta:
        verbose_name = 'Статистика по ответам пользователя'
        verbose_name_plural = 'Статистики по ответам пользователя'

from allauth.account.adapter import DefaultAccountAdapter


class CindiAccountAdapter(DefaultAccountAdapter):
    def save_user(self, request, user, form, commit=True):
        user = super().save_user(request, user, form, False)
        user.is_staff = form.cleaned_data['is_staff']
        if commit:
            user.save()
        return user

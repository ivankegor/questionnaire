from django.core.validators import BaseValidator
from django.utils.deconstruct import deconstructible


@deconstructible
class NotEqualValidator(BaseValidator):
    message = 'Значени не может быть равно %(limit_value)s.'
    code = 'not_equal'

    def compare(self, a, b):
        return a == b
